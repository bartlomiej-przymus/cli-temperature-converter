#!/usr/bin/env php
<?php

use App\Config\TempConverterConfig;
use Webmozart\Console\ConsoleApplication;

if (file_exists($autoload = __DIR__.'/../../../autoload.php')) {
    require_once $autoload;
} else {
    require_once __DIR__.'/vendor/autoload.php';
}

try {
    $cli = new ConsoleApplication(new TempConverterConfig);
    $cli->run();
} catch (Exception $exception) {
    echo $exception->getMessage();
}
