# CLI Temperature Converter

## Installation

- Clone the repo
- run `composer install`
- run application with following commands:
    - to convert fahrenheit to celsius:
        `php run.php temp-convert 20 -c`
    - to convert celsius to fahrenheit:
        `php run.php temp-convert 40 -f`

## Testing

- run test with "composer run-script tests"