<?php

namespace App\Handlers;
use App\Repositories\TempConverterRepository;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\Command\Command;
use Webmozart\Console\Api\IO\IO;

class TempConverterHandler
{
    private TempConverterRepository $converter;

    public function __construct()
    {
        $this->converter = new TempConverterRepository;
    }

    public function handle(Args $args, IO $io, Command $command): int
    {
        if ($args->isArgumentSet('temp')) {
            $temp = $args->getArgument('temp');
            $converter = $this->converter;

            if ($args->isOptionSet('celsius')) {
                $result = $converter->setDirection(true)
                    ->convert($temp);
                $io->writeLine("$temp degrees Celsius is: $result Fahrenheit");
            } else if ($args->isOptionSet('fahrenheit')) {
                $result = $converter->setDirection(false)
                    ->convert($temp);
                $io->writeLine("$temp degrees Fahrenheit is: $result Celsius");
            } else {
                $io->writeLine('Please provide valid scale conversion option either -f or -c');
                return 1;
            }

            $io->writeLine('Done');

            return 0;
        }

        return 1;
    }
}
