<?php

namespace App\Config;

use App\Handlers\TempConverterHandler;
use Webmozart\Console\Api\Args\Format\Argument;
use Webmozart\Console\Api\Args\Format\Option;
use Webmozart\Console\Config\DefaultApplicationConfig;

class TempConverterConfig extends DefaultApplicationConfig
{
    protected function configure(): void
    {
        parent::configure();

        $this
            ->setName('temperature-converter')
            ->setDisplayName('Temperature Converter')
            ->setVersion('1.0.0')
            ->beginCommand('temp-convert')
                ->setDescription('Converts temperature')
                ->setHandler(new TempConverterHandler)
                ->addArgument(
                    'temp',
                    Argument::REQUIRED,
                    'Temperature value to convert'
                )
                ->addOption(
                    'fahrenheit',
                    'f',
                    Option::NO_VALUE,
                    'Converts given temperature to Fahrenheit scale'
                )
                ->addOption(
                    'celsius',
                    'c',
                    Option::NO_VALUE,
                    'Converts given temperature to Celsius scale'
                )
            ->end();
    }
}
