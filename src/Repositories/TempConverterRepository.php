<?php

namespace App\Repositories;

class TempConverterRepository
{
    public bool $direction;

    /**
     * Sets direction of temperature conversion.
     *
     * @param bool $direction If true Celsius to Fahrenheit false assumes reverse.
     */
    public function setDirection(bool $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    private function calcFahrenheit(int $celsius): int
    {
        return ($celsius * 9/5) + 32;
    }

    private function calcCelsius(int $fahrenheit): int
    {
        return (5 * ($fahrenheit - 32)) / 9;
    }

    public function convert(float $temp): float
    {
        if($this->direction) {
            return $this->calcFahrenheit($temp);
        }

        return $this->calcCelsius($temp);
    }
}
