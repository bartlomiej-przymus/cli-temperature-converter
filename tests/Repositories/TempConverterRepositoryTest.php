<?php

namespace Tests\Repositories;

use App\Repositories\TempConverterRepository;
use PHPUnit\Framework\TestCase;

class TempConverterRepositoryTest extends TestCase
{
    public function providesData(): array
    {
        return [
            [20, 68],
            [10, 50],
            [5, 41],
            [-25, -13],
            [-20, -4],
        ];
    }

    /** @test */
    public function can_set_direction(): void
    {
        $converter = new TempConverterRepository;
        $converter->setDirection(true);
        $this->assertTrue($converter->direction);

        $converter->setDirection(false);
        $this->assertFalse($converter->direction);
    }

    /**
     * @test
     * @dataProvider providesData
     */
    public function can_calculate_fahrenheit_from_celsius(
        int $celsius,
        int $fahrenheit
    ): void
    {
        $result = (new TempConverterRepository)
            ->setDirection(true)
            ->convert($celsius);

        $this->assertEquals($fahrenheit, $result);
    }

    /**
     * @test
     * @dataProvider providesData
     */
    public function can_calculate_celsius_from_fahrenheit(
        int $celsius,
        int $fahrenheit
    ): void
    {
        $result = (new TempConverterRepository)
            ->setDirection(false)
            ->convert($fahrenheit);

        $this->assertEquals($celsius, $result);
    }
}

